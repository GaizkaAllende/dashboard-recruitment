# i2C

### Front end

Please run 'yarn dev' or 'npm run dev' and browse http://localhost:3000 for the example dashboard of weekly data. 

### Back end

Modify the config file api/config.json with the database details

Please run 'yarn server' or 'npm run server' for the backend express api. 

### Database

There needs to be a MariaDB server running 

I create a user called 'nectar' with passport 'nectar' and a table called 'Weekly': 

CREATE USER 'nectar'@'localhost' IDENTIFIED BY 'nectar';

create database nectar;

GRANT ALL PRIVILEGES ON nectar.* TO 'nectar'@'localhost';

use nectar;

CREATE TABLE Weekly(PRODUCT VARCHAR(256) NOT NULL, WEEK_COMMENCING DATE NOT NULL, EXPOSED BIGINT NOT NULL, CONTROL BIGINT NOT NULL);

Then I imported the CVS file into the 'Weekly' table using the following command (change file path)

LOAD DATA LOCAL INFILE '/home/gaizka/Dev/dashboard-recruitment/data/weekly.csv' INTO TABLE Weekly fields terminated by ',' lines terminated by '\n' IGNORE 1 lines;



