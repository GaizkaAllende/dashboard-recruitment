const mariadb = require('mariadb');
const express = require('express')
const cors = require('cors');
const config = require('./config.json');

const app = express()

app.use(function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  next();
});

app.get(
  '/weekly',
  async (req, res) => {

    const product  = req.query.product.toUpperCase();
    let rows = [];
    try {
      const conn = await mariadb.createConnection(
        {
          host: config.database.host,
          user: config.database.user,
          password: config.database.password,
          database: config.database.database,
        }
      );
      rows = await conn.query(
        `SELECT WEEK_COMMENCING, EXPOSED, CONTROL from Weekly WHERE PRODUCT = "${product}"`,
        [2]
      );
      conn.end();
    } catch(err) {
      console.log(err);
    };

    const data = rows
      .map(
        ({ WEEK_COMMENCING, EXPOSED, CONTROL }) => ({
          weekCommencing: WEEK_COMMENCING,
          exposed: EXPOSED,
          control: CONTROL,
        })
      );
    res.status(200).json(data);
  }
)

app.listen(config.port, () => console.log(`Listening at http://localhost:${config.port}`))
