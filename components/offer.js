import {useState} from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Grid from '@material-ui/core/Grid';
import CircularProgress from '@material-ui/core/CircularProgress';
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';
import TabPanel from './tabPanel';

import useWeeklyData from '../hooks/useWeeklyData';
import Panel from './panel';
import WeeklyTable from './weeklyTable';
import WeeklyBarChart from './weeklyBarChart';
import WeeklyLineChart from './weeklyLineChart';

const useStyles = makeStyles({
  loader: {
    height: '80%',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
  }
});

export default function Aisle() {
  const product = 'offer';
  const data = useWeeklyData(product);
  const [tab, setTab] = useState(0);
  const classes = useStyles();

  if (data.length === 0) {
    return (
      <Grid item xs={12} md={12}>
        <Panel product={product}>
          <div className={classes.loader}>
            <CircularProgress />
          </div>
        </Panel>
      </Grid>
    );
  }

  return (
    <>
      <Grid item xs={12} md={6}>
        <Panel product={product}>
           <WeeklyTable
              product={product}
              data={data}
            />
        </Panel>
      </Grid>
      <Grid item xs={12} md={6}>
        <Panel product={product}>
          <Tabs
            value={tab}
            onChange={(e, value) => setTab(value)}
            aria-label="simple tabs example"
          >
            <Tab label="Bar chart" />
            <Tab label="Line chart" />
          </Tabs>
          <TabPanel value={tab} index={0}>
            <WeeklyBarChart
              product={product}
              data={data}
            />
          </TabPanel>
          <TabPanel value={tab} index={1}>
            <WeeklyLineChart
              product={product}
              data={data}
            />
          </TabPanel>
        </Panel>
      </Grid>
    </>
  );
}
