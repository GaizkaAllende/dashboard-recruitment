import { makeStyles } from '@material-ui/core/styles';
import Paper from '@material-ui/core/Paper';
import Typography from '@material-ui/core/Typography';

import theme from '../theme';

const useStyles = makeStyles({
  root: {
    minHeight: 450,
  },
  paper: {
    padding: theme.spacing(2),
    textAlign: "left",
    color: theme.palette.text.secondary,
    height: 530,
    maxHeight: 530,
  },
  content: {
    height: '80%',
  }
});

export default function Panel({
  product,
  children
}) {
  const classes = useStyles();
  return (
    <Paper
      classes={{
        root: classes.root,
      }}
      className={classes.paper}
    >
      <div>
        <Typography variant="subtitle1" gutterBottom>
          {product}
        </Typography>
      </div>
      <div className={classes.content}>
        {children}
      </div>
    </Paper>
  );
}
