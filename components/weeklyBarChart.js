import ResponsiveContainer from 'recharts/lib/component/ResponsiveContainer';
import BarChart from 'recharts/lib/chart/BarChart';
import Bar from 'recharts/lib/cartesian/Bar';
import XAxis from 'recharts/lib/cartesian/XAxis';
import Tooltip from 'recharts/lib/component/Tooltip';


export default function WeeklyLineChart({
  product,
  data,
}) {
  return (
    <ResponsiveContainer width="99%" height={400}>
      <BarChart data={data}>
        <XAxis dataKey="weekCommencing"/>
        <Tooltip/>
        <Bar dataKey="exposed" stackId="a" fill="green" />
        <Bar dataKey="control" stackId="a" fill="blue" />
      </BarChart>
    </ResponsiveContainer>
  );
}
