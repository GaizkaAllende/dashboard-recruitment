import { ResponsiveContainer, LineChart, Line, XAxis, YAxis, ReferenceLine, ReferenceArea,
    ReferenceDot, Tooltip, CartesianGrid, Legend, Brush, ErrorBar, AreaChart, Area,
    Label, LabelList } from 'recharts';

const data02 = [
  { name: 'Page A', uv: 300, pv: 2600, amt: 3400 },
  { name: 'Page B', uv: 400, pv: 4367, amt: 6400 },
  { name: 'Page C', uv: 300, pv: 1398, amt: 2400 },
  { name: 'Page D', uv: 200, pv: 9800, amt: 2400 },
  { name: 'Page E', uv: 278, pv: 3908, amt: 2400 },
  { name: 'Page F', uv: 189, pv: 4800, amt: 2400 },
  { name: 'Page G', uv: 189, pv: 4800, amt: 2400 },
];


export default function WeeklyLineChart({
  data,
  product,
}) {
	const opacity = 1;
  return (
		<LineChart width={400} height={400} data={data} syncId="test">
			<YAxis type="number" yAxisId={0} />
			<YAxis type="number" yAxisId={1} />
			<XAxis dataKey="weekCommencing" />
			<Tooltip trigger="click" />
			<CartesianGrid stroke="#f5f5f5" fill="#e6e6e6" />
      <Line dataKey='exposed' stroke='#ff7300' strokeWidth={2} yAxisId={0}/>
      <Line dataKey='control' stroke='#ff7300' strokeWidth={2} yAxisId={0}/>
		</LineChart>
  );
}
