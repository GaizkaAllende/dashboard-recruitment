import { makeStyles } from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';

const useStyles = makeStyles({
  table: {
    //minWidth: 650,
  },
  container: {
    maxHeight: 450,
  },
});


export default function WeeklyTable({
  product,
  data,
}) {
  const classes = useStyles();
  return (
    <>
      <TableContainer
        className={classes.container}
        component={Paper}
      >
        <Table
          stickyHeader
          className={classes.table}
          aria-label="simple table"
        >
          <TableHead>
            <TableRow>
              <TableCell>Week commencing</TableCell>
              <TableCell>Exposed</TableCell>
              <TableCell>Control</TableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            {
              data && data.map(
                row => {
                  return (
                    <TableRow key={row.weekCommencing}>
                      <TableCell component="th" scope="row">
                        {row.weekCommencing}
                      </TableCell>
                      <TableCell>{row.exposed}</TableCell>
                      <TableCell>{row.control}</TableCell>
                    </TableRow>
                  )
                }
              )
           }
          </TableBody>
        </Table>
      </TableContainer>
    </>
  );
}
