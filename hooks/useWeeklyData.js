import { useEffect, useState } from 'react';

export default function useWeeklyData(product) {

  const [weeklyData, setWeeklyData] = useState([]);

  useEffect(
    () => {
      async function fetchWeeklyData() {
        const data = await fetch(`http://localhost:4000/weekly?product=${product}`);
        const json = await data.json();
        setWeeklyData(json);
      }
      fetchWeeklyData();
    },
    [],
  );

  return weeklyData;
}
