import { useState } from 'react';

import { makeStyles } from '@material-ui/core/styles';
import CssBaseline from '@material-ui/core/CssBaseline';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import Paper from '@material-ui/core/Paper';

import WeeklyTable from '../components/weeklyTable';
import TabPanel from '../components/tabPanel';

import Aisle from '../components/aisle';
import Brand from '../components/brand';
import Offer from '../components/offer';

import theme from '../theme';

const useStyles = makeStyles({
  root: {
    flexGrow: 1,
    backgroundColor: theme.palette.grey["100"],
    //backgroundColor: '#6da8c3',
    overflow: "hidden",
    backgroundSize: "cover",
    backgroundPosition: "0 400px",
    paddingBottom: 200
  },
  grid: {
    width: 1200,
    margin: `0 ${theme.spacing(2)}px`,
    [theme.breakpoints.down("sm")]: {
      width: "calc(100% - 20px)"
    }
  },
  loadingState: {
    opacity: 0.05
  },
  paper: {
    padding: theme.spacing(3),
    textAlign: "left",
    color: theme.palette.text.secondary
  },
  topBar: {
    display: "flex",
    justifyContent: "space-between",
    alignItems: "center"
  },
});

function Dashboard() {
  const classes = useStyles();
  return (
      <>
        <CssBaseline />
        <div className={classes.root}>
          <Grid container justify="center">
            <Grid
              spacing={5}
              alignItems="center"
              justify="center"
              container
              className={classes.grid}
            >
              <Grid item xs={12}>
                <Paper className={classes.paper}>
                  <div className={classes.topBar}>
                    <div className={classes.block}>
                      <Typography variant="h6" gutterBottom>
                        Dashboard
                      </Typography>
                      <Typography variant="body1">
                        Weekly data visualization.
                      </Typography>
                    </div>
                  </div>
                </Paper>
              </Grid>
              <Aisle />
              <Brand />
              <Offer />
            </Grid>
          </Grid>
        </div>
      </>
  );
}

export default Dashboard;
